import os, random, hashlib, datetime, shutil
from shutil import copyfile

if __name__ == '__main__':

    root_path = os.path.dirname(os.path.abspath(__file__))
    random_string = hashlib.sha224(str(datetime.datetime.now()).encode('utf-8')).hexdigest()

    release_path = root_path + '\\release\\' + random_string + '\\'
    data_path = root_path + '\\data\\'
    good_images_path = root_path + '\\data\\good\\'
    bad_images_path = root_path + '\\data\\bad\\'

    try:
        os.stat(release_path)
    except:
        os.mkdir(release_path)
        os.mkdir(release_path + '\\good\\')
        os.mkdir(release_path + '\\bad\\')

    path_to_good_dat = 'good.dat'

    '''
    path_to_good_dat = input("Enter the path to good.dat file: ")
    '''
    count_of_good_samples = input("Count of good samples: ")
    count_of_bad_samples = input("Count of bad samples: ")

    with open(root_path + '\\data\\' + path_to_good_dat) as f:
        good_from_file = f.readlines()

    random.shuffle(good_from_file)

    result_good_list = []
    result_bad_list = []

    i = 0
    for item in good_from_file:
        if i < int(count_of_good_samples):
            test = item.split('  ')
            if os.path.exists(data_path + test[0]):
                j = test[0].split('/')
                copyfile(data_path + test[0], release_path + 'good\\' + j[1])
                result_good_list.append(item)
                i += 1

    with open(release_path + "good.dat", "a+") as f:
        for item in result_good_list:
            f.write(item)

    bad_dir = os.listdir(bad_images_path)
    random.shuffle(bad_dir)
    i = 0
    for file in bad_dir:
        if i < int(count_of_bad_samples):
            if file.endswith(".JPG"):
                result_bad_list.append(file)
                copyfile(bad_images_path + file, release_path + 'bad\\' + file)
                i += 1

    with open(release_path + "bad.dat", "a+") as f:
        for item in result_good_list:
            f.write("%s\n" % item)

    shutil.make_archive(str(datetime.datetime.now()).replace(' ', '_').replace(':', '-')+'-release.zip', 'zip', release_path)